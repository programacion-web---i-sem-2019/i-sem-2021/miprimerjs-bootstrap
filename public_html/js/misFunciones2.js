/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function calcularNotas()
{

    let datos = document.getElementsByName("datos");
//alert("el vector contiene:"+datos.length+","+datos[0].value);
    let respuesta = document.getElementById("respuesta");
    if (!validar(datos))
    {
        respuesta.innerHTML = "<b>Error de algún dato !!!!</b>";
    } else
    {
        let promedio=calcularPromedio(datos);
        respuesta.innerHTML = "<b>Su definitiva es: "+promedio.toFixed(1)+"</b>";
        crearTabla(datos,promedio);
        //llamar la función que crea el gráfico de barra:
        crearGraficoBarra(datos);
        //llamar la función que crea el gráfico de torta:
        crearGraficoTorta(datos);
    }


}


function calcularPromedio(datos)
{

    let p1 = parseFloat(datos[1].value);
    let p2 = parseFloat(datos[2].value);
    let p3 = parseFloat(datos[3].value);
    let exm = parseFloat(datos[4].value);
    let promedio = ((p1 + p2 + p3) / 3) * 0.7 + exm * 0.3;
    return promedio;
}

//Datos es un componente html
function validar(datos)
{
    for (const elemento of datos)
    {
        if (elemento.getAttribute("type") === "number")
        {

            let valor = parseFloat(elemento.value);
            let min = parseInt(elemento.getAttribute("min"));
            let max = parseInt(elemento.getAttribute("max"));
            if (valor < min || valor > max)
            {
                elemento.style.color="red";
                return false;

            }

        }

    }
    return true;
}