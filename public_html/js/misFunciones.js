/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function calcular()
{
    // Leer los datos
    let nombre = document.getElementById("nombre").value;
    let p1 = parseFloat(document.getElementById("p1").value);
    let p2 = parseFloat(document.getElementById("p2").value);
    let p3 = parseFloat(document.getElementById("p3").value);
    let exm = parseFloat(document.getElementById("exm").value);
    let promedio = ((p1 + p2 + p3) / 3) * 0.7 + exm * 0.3;
    //Procesar datos
    let rta = "Estudiante :" + nombre + ", su definitiva es:" + promedio.toFixed(1);
    //Mostrar datos
    //Ubicarnos sobre el componente de impresión:
    let respuesta = document.getElementById("respuesta");
    respuesta.innerHTML = rta;
}
