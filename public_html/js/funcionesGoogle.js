/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function crearTabla(datos, promedio)
{
    var data = new google.visualization.DataTable();
    //Creando columnas: 
    data.addColumn("string", "Nombres Completos");
    data.addColumn("number", "Previo 1");
    data.addColumn("number", "Previo 2");
    data.addColumn("number", "Previo 3");
    data.addColumn("number", "Examen");
    data.addColumn("number", "Definitiva");
    
    /*
     *  let mensajes = ["Nombres", "Previo1", "Previo2", "Previo3", "Examen","Definitiva"];
     *  
     *  for (i = 0; i < mensajes.length; i++) {
     if (i == 0) 
     data.addColumn("string", mensajes[i]);
     else 
     data.addColumn("number", mensajes[i]);
     }
     * 
     */
    //Creando filas:
    data.addRows(1);
    data.setCell(0, 0, datos[0].value);
    data.setCell(0, 1, datos[1].value);
    data.setCell(0, 2, datos[2].value);
    data.setCell(0, 3, datos[3].value);
    data.setCell(0, 4, datos[4].value);
    data.setCell(0, 5, promedio.toFixed(1));
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

}




function crearTablaOriginal()
{
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('number', 'Salary');
    data.addColumn('boolean', 'Full Time Employee');
    data.addRows([
        ['Mike', {v: 10000, f: '$10,000'}, true],
        ['Jim', {v: 8000, f: '$8,000'}, false],
        ['Alice', {v: 12500, f: '$12,500'}, true],
        ['Bob', {v: 7000, f: '$7,000'}, true]
    ]);

    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

}




function crearGraficoBarra(datos)
{
    let mensajes = ["Nombres", "Previo1", "Previo2", "Previo3", "Examen"];
    var data = new google.visualization.DataTable();
    data.addColumn("string", "Descripción")
    data.addColumn("number", "Nota");
    data.addRows(4);
    let m = "";
    for (i = 1; i < datos.length; i++)
    {
        m += "\n" + datos[i].value + "-" + mensajes[i];
        data.setCell(i - 1, 0, mensajes[i]);
        data.setCell(i - 1, 1, datos[i].value);

    }
    alert(m);
    var options = {
        chart: {
            title: 'Notas',
            subtitle: 'Previos',
        },
        bars: 'horizontal' // Required for Material Bar Charts.
    };
    var chart = new google.charts.Bar(document.getElementById('barchart_material'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
}


function crearGraficoTorta(datos)
{
    let mensajes = ["Nombres", "Previo1", "Previo2", "Previo3", "Examen"];
    var data = new google.visualization.DataTable();
    data.addColumn("string", "Descripción");
    data.addColumn("number", "Nota");
    data.addRows(4);
    let m = "";
    for (i = 1; i < datos.length; i++)
    {
        m += "\n" + datos[i].value + "-" + mensajes[i];
        data.setCell(i - 1, 0, mensajes[i]);
        data.setCell(i - 1, 1, datos[i].value);

    }
    alert(m);

    var options = {
        title: 'Estadística de Notas',
        is3D: true,
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    chart.draw(data, options);


}
